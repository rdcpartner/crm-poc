package com.rdcpartner.crm.domain;

import org.springframework.data.annotation.Id;

public class Order {

    @Id
    private String id;

    private boolean active;

    private String staticIpAddress;

    private String customerId;

    private String productId;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getStaticIpAddress() {
		return staticIpAddress;
	}

	public void setStaticIpAddress(String staticIpAddress) {
		this.staticIpAddress = staticIpAddress;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", active=" + active + ", staticIpAddress=" + staticIpAddress + ", customerId="
				+ customerId + ", productId=" + productId + "]";
	}
}
