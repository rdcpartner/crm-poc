package com.rdcpartner.crm.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rdcpartner.crm.domain.Product;
import com.rdcpartner.crm.exception.ProductNotFoundException;
import com.rdcpartner.crm.service.ProductService;

@Controller
@Path("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @POST
    @Path("/")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Product create(Product product) {
        try {
            return productService.save(product);
        } catch (ProductNotFoundException e) {
            throw new WebApplicationException("Product not found", NOT_FOUND);
        }
    }

    @PUT
    @Path("/")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Product update(Product product) {
        try {
            return productService.save(product);
        } catch (ProductNotFoundException e) {
            throw new WebApplicationException("Product not found", NOT_FOUND);
        }
    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("/{productId}")
    public Product get(@PathParam("productId") String productId) {
        Optional<Product> product = productService.get(productId);
        if (product.isPresent()) {
            return product.get();
        } else {
            throw new WebApplicationException("Product not found", NOT_FOUND);
        }
    }

    @GET
    @Path("/")
    @Produces(APPLICATION_JSON)
    public List<Product> list() {
        return productService.listAll();
    }
}
