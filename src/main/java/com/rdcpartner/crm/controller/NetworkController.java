package com.rdcpartner.crm.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rdcpartner.crm.service.NetworkService;

@Controller
@Path("/network")
public class NetworkController {

	@Autowired
	private NetworkService networkService;
	
    @GET
    @Produces(APPLICATION_JSON)
    @Path("/generate-ip")
    public Map<String, String> generate() {
    	Map<String, String> map = new LinkedHashMap<>(1);
    	map.put("ipv4", networkService.generateIp());
    	return map;
    }
}
