package com.rdcpartner.crm.controller;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import java.util.List;
import java.util.Optional;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.rdcpartner.crm.domain.Order;
import com.rdcpartner.crm.exception.OrderNotFoundException;
import com.rdcpartner.crm.service.OrderService;

@Controller
@Path("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @POST
    @Path("/")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Order save(Order order) {
        try {
            return orderService.save(order);
        } catch (OrderNotFoundException e) {
            throw new WebApplicationException("Order not found", NOT_FOUND);
        }
    }

    @PUT
    @Path("/")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Order update(Order order) {
        try {
            return orderService.save(order);
        } catch (OrderNotFoundException e) {
            throw new WebApplicationException("Order not found", NOT_FOUND);
        }
    }

    @GET
    @Produces(APPLICATION_JSON)
    @Path("/{orderId}")
    public Order get(@PathParam("orderId") String orderId) {
        Optional<Order> oder = orderService.get(orderId);
        if (oder.isPresent()) {
            return oder.get();
        } else {
            throw new WebApplicationException("Order not found", NOT_FOUND);
        }
    }

    @GET
    @Path("/")
    @Produces(APPLICATION_JSON)
    public List<Order> list() {
        return orderService.listAll();
    }
}
