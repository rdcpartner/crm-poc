package com.rdcpartner.crm.exception;

public class ProductNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 6895714303000544911L;
}
