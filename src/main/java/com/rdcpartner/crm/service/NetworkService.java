package com.rdcpartner.crm.service;

import java.util.Random;

import org.springframework.stereotype.Component;

@Component
public class NetworkService {

    private static final String DEFAULT_MASK = "255.0.0.0";

    public String generateIp() {
        return generateRandomIP(DEFAULT_MASK, 10);
    }

    private int findRange(int mask) {
        int x = 8 - mask;
        int sum = 0;
        for (int i = 0; i < x; i++) {
            sum += Math.pow(2, i);
        }
        return sum;
    }

    private int findFixedPart(String prefix, int i) {
        String f = prefix.split("\\.")[i];
        return Integer.valueOf(f);
    }

    private String generateRandomIP(String prefix, Integer mask) {
        String IP = "";
        Random r = new Random();
        if (mask < 8) {
            IP = (findFixedPart(prefix, 0) + r.nextInt(findRange(mask))) + "." + r.nextInt(256) + "." + r.nextInt(256)
                    + "." + r.nextInt(256);
        } else if (mask > 7 && mask < 16) {
            IP = findFixedPart(prefix, 0) + "." + (findFixedPart(prefix, 1) + r.nextInt(findRange(mask - 8))) + "."
                    + r.nextInt(256) + "." + r.nextInt(256);
        } else if (mask > 15 && mask < 24) {
            IP = findFixedPart(prefix, 0) + "." + findFixedPart(prefix, 1) + "."
                    + (findFixedPart(prefix, 2) + r.nextInt(findRange(mask - 16))) + "." + r.nextInt(256);
        } else if (mask > 23 && mask < 33) {
            IP = findFixedPart(prefix, 0) + "." + findFixedPart(prefix, 1) + "." + findFixedPart(prefix, 2) + "."
                    + (findFixedPart(prefix, 3) + r.nextInt(findRange(mask - 24)));
        }
        return IP;
    }
}
