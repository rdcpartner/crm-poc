package com.rdcpartner.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rdcpartner.crm.domain.Product;
import com.rdcpartner.crm.exception.ProductNotFoundException;
import com.rdcpartner.crm.repository.ProductMongoRepository;

@Service
public class ProductService {

    @Autowired
    private ProductMongoRepository productMongoRepository;

    public Product save(Product product) {
        boolean isNew = product.getId() == null;
        Product inserted = null;
        if (isNew) {
            inserted = productMongoRepository.insert(product);
        } else {
            Optional<Product> found = productMongoRepository.findById(product.getId());
            if (found.isPresent()) {
                inserted = productMongoRepository.save(product);
            } else {
                throw new ProductNotFoundException();
            }
        }
        return inserted;
    }

    public List<Product> listAll() {
        return productMongoRepository.findAll();
    }

	public Optional<Product> get(String productId) {
		return productMongoRepository.findById(productId);
	}
}
