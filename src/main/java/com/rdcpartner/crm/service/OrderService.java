package com.rdcpartner.crm.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rdcpartner.crm.domain.Order;
import com.rdcpartner.crm.exception.ProductNotFoundException;
import com.rdcpartner.crm.repository.OrderMongoRepository;

@Service
public class OrderService {

    @Autowired
    private OrderMongoRepository orderMongoRepository;

    public Order save(Order order) {
        boolean isNew = order.getId() == null;
        Order inserted = null;
        if (isNew) {
            inserted = orderMongoRepository.insert(order);
        } else {
            Optional<Order> found = orderMongoRepository.findById(order.getId());
            if (found.isPresent()) {
                inserted = orderMongoRepository.save(order);
            } else {
                throw new ProductNotFoundException();
            }
        }
        return inserted;
    }

    public List<Order> listAll() {
        return orderMongoRepository.findAll();
    }

	public Optional<Order> get(String orderId) {
		return orderMongoRepository.findById(orderId);
	}
}
