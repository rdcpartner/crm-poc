package com.rdcpartner.crm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rdcpartner.crm.domain.Order;

@Repository
public interface OrderMongoRepository extends MongoRepository<Order, String> {

}
