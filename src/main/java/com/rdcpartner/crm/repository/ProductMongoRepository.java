package com.rdcpartner.crm.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.rdcpartner.crm.domain.Product;

@Repository
public interface ProductMongoRepository extends MongoRepository<Product, String> {

}
