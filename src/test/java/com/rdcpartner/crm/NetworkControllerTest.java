package com.rdcpartner.crm;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import io.restassured.response.Response;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
public class NetworkControllerTest {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }
    
	@Test
	@SuppressWarnings("unchecked")
    public void testInsertOrder() {
        Response response = given()
                            .contentType(JSON)
                            .get("/network/generate-ip")
                            .then()
                            .statusCode(200)
                            .extract()
                            .response();

        Map<String, String> map = response.as(Map.class);
        assertNotNull(map);
        assertTrue(map.containsKey("ipv4"));
        assertNotNull(map.get("ipv4"));
    }
}
