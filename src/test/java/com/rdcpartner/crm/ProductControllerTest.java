package com.rdcpartner.crm;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import com.rdcpartner.crm.domain.Product;

import io.restassured.mapper.TypeRef;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
public class ProductControllerTest {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }
    
    @Test
    public void testInsertCatalog() {
        Product product = new Product();
        product.setName("fiber");

        Response response = given()
                            .contentType(JSON)
                            .body(product)
                            .post("/products")
                            .then()
                            .statusCode(200)
                            .extract()
                            .response();

        Product inserted = response.as(Product.class);
        assertNotNull(inserted);
        assertEquals(product.getName(), inserted.getName());

        Response responseCatalogList = given()
                                        .contentType(JSON)
                                        .get("/products")
                                        .then()
                                        .statusCode(200)
                                        .extract()
                                        .response();

        List<Product> products = responseCatalogList.as(new TypeRef<List<Product>>() {});
        assertNotNull(products);
        assertTrue(products.size() > 0);
    }
}
