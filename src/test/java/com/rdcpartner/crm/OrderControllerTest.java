package com.rdcpartner.crm;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import com.rdcpartner.crm.domain.Order;

import io.restassured.mapper.TypeRef;
import io.restassured.response.Response;

@SpringBootTest(webEnvironment =  RANDOM_PORT)
public class OrderControllerTest {

    @LocalServerPort
    private int port;

    @BeforeEach
    public void before() {
        baseURI = "http://localhost:" + port;
    }
    
    @Test
    public void testInsertOrder() {
        Order order = new Order();
        order.setActive(true);
        order.setStaticIpAddress("192.168.2.1");

        Response response = given()
                            .contentType(JSON)
                            .body(order)
                            .post("/orders")
                            .then()
                            .statusCode(200)
                            .extract()
                            .response();

        Order inserted = response.as(Order.class);
        assertNotNull(inserted);
        assertEquals("192.168.2.1", order.getStaticIpAddress());

        Response responseOrderList = given()
                                        .contentType(JSON)
                                        .get("/orders")
                                        .then()
                                        .statusCode(200)
                                        .extract()
                                        .response();

        List<Order> products = responseOrderList.as(new TypeRef<List<Order>>() {});
        assertNotNull(products);
        assertTrue(products.size() > 0);
    }
}
